//на матрицы структур : есть матрица структур, 12 строк, 31 строка(по месяцам), 
//структура хранит в себе: 
//1) на какую сумму товаров куплено 
//2) на какую сумму товара продано 
//3) сколько посетителей в магазине
//4) сколько посетителей вышли без покупок (можно рандомно задавать)
//найти выручку(чистую прибыль) за какое - то число (с клавиатуры).
//найти средний чек за день.
//найти процент посетителей которые купили что-либо.

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct mag
{
	int buy;
	int sell;
	int people;
	int bad_people;
};

struct mag** matr_alloc(int row, int col)
{
	struct mag** matr = NULL;
	matr = (struct mag**) calloc(sizeof(struct mag*), row);
	if (matr == NULL)
		perror("error: no memory\n");
	for(int i = 0; i < row; i++)
		matr[i] = (struct mag*) calloc(sizeof(struct mag), col);
	return matr;
}

void add_matr(struct mag** matr, int row, int col)
{
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{

			matr[i][j].buy = rand() % 50000;
			matr[i][j].sell = rand() % 100000;
			matr[i][j].people = rand() % 500;											
			matr[i][j].bad_people = rand() % (matr[i][j].people + 1);	
		}
	}
}
void print_one_day(struct mag** matr, int row, int col, int mes, int day) 	// ну и че за пиздец? ввести mes day 12 31 (ответ: опять хуйня с [n-1]). fixed
{
	
	printf("buy = %d\n", matr[mes - 1][day - 1].buy);
	printf("sell = %d\n", matr[mes - 1][day - 1].sell);
	printf("people = %d\n", matr[mes - 1][day - 1].people);
	printf("bad_people = %d\n", matr[mes - 1][day - 1].bad_people);
}
			

void free_matr(struct mag** matr, int row)
{
	for(int i = 0; i < row; i++)
	{
		free(matr[i]);
	}
	free(matr);
}

int money(struct mag** matr, int row, int col, int mes, int day)
{
	return (matr[mes - 1][day - 1].sell - matr[mes - 1][day - 1].buy);		
}

float average(struct mag** matr, int row, int col, int mes, int day)
{	
	return (matr[mes - 1][day - 1].sell / (matr[mes - 1][day - 1].people - matr[mes - 1][day - 1].bad_people));
}

float percent(struct mag** matr, int row, int col, int mes, int day)
{
	return (((matr[mes - 1][day - 1].people - matr[mes - 1][day - 1].bad_people) * 100 / (matr[mes - 1][day - 1].people)));
}

int main()
{
	int mes = 12;
	int day = 31;

	struct mag** matr = matr_alloc(mes, day);

	add_matr(matr, mes, day);

	int mes_temp;
	int day_temp;
	printf("введите mes и day\n");
	scanf("%d %d", &mes_temp, &day_temp);
	if (mes_temp > 12 || day_temp > 31)
	{
		printf("Введите правильно mes и day\n");
	}
	else
	{
		print_one_day(matr, mes, day, mes_temp, day_temp);

		int m = money(matr, mes, day, mes_temp, day_temp);
		printf("money = %d\n", m);
		float a = average(matr, mes, day, mes_temp, day_temp);
		printf("average = %f\n", a);
		float p = percent(matr, mes, day, mes_temp, day_temp);
		printf("percent = %f%\n", p);
	}
	free_matr(matr, mes);
	return 0;
}
