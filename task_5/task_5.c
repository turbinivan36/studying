//найти в сторке сам. длинное сочетание символов

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void print_str(char* str)
{
	int i = 0;
	while(strcmp(&str[i], "\0"))
	{
		printf("%c", str[i]);
		i = i + 1;
	}
	printf("\n");
}

int max_elem(char* str)
{
	int i = 0;
	int max = 0;
	int curr = 0;
	while(strcmp(&str[i], "\0"))
	{
		curr++;
		if(str[i] == ' ')
		{
			curr--;
			if(curr > max)
			{
				max = curr;
			}
			curr = 0;
		}
		i++;
	}
	return max;
}

int main()
{
	int n = 50;

	char* str = NULL;
	str = (char*) calloc(sizeof(char), n);
	if(str == NULL)
		printf("no memes");

	gets(str);
	print_str(str);
	int max = max_elem(str);
	printf("max = %d\n", max);
	//printf("%s", str);
	return 0;
}