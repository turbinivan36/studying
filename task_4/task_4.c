//Создать матрицу м на н и вторую, м и н - с клавиатуры
//зеркально отобразить элементы относительно главной диагонали если она квадратная
//если она не кв, то перемножить две матрицы
//меню через свич кейс
#include <stdio.h>
#include <stdlib.h>

int** matr_alloc(int row, int col)
{	
	int** matr = (int**) calloc(sizeof(int*), row);
	for (int i = 0; i < row; i++)
	{
		matr[i] = (int*) calloc(sizeof(int), col);
	}
	return matr;
}

void add_elements_in_matr(int** matr, int row, int col)
{
	printf("press number what u want\n");
	for (int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{
			scanf("%d", &matr[i][j]);
		}
	}
}

void print_matr(int** matr, int row, int col)
{
	printf("\n \n");
	for (int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{
			printf("%d   ", matr[i][j]);
		}
		printf("\n \n");
	}
	printf("\n");
}

void free_matr(int** matr, int row, int col)
{
	for (int i = 0; i < row; i++)
	{
		free(matr[i]);
	}
	free(matr);
}

void switch_elements(int** matr, int row, int col)
{
	int temp;
	if(row != col)
		printf("wrong matrix for switching\n");
	else
	{
		for (int i = 0; i < row; i++)
		{
			for(int j = 0; j < i; j++)
			{
				temp = matr[i][j];
				matr[i][j] = matr[j][i];
				matr[j][i] = temp;
			}
		} 
	}
}

void multiplying_elements(int** matr1, int** matr2, int row1, int col1, int row2, int col2)
{
	int** matr = matr_alloc(row1, col2);
	if(col1 != row2)
		printf("wrong task\n");
	else 
	{	
		
		for(int i = 0; i < row1; i++)
		{
			for(int j = 0; j < col2; j++)
			{
				for(int k = 0; k < col1; k++)
				{
					matr[i][j] = matr[i][j] + matr1[i][k] * matr2[k][j];
				}
			}
		}
		print_matr(matr, row1, col2);
	}
	free_matr(matr, row1, col2);
}

int main()
{	
	int row1;
	int col1;
	int row2;
	int col2;

	printf("row and col of 1-st matr - ?\n");
	scanf("%d %d", &row1, &col1);
	printf("row and col of 2-nd matr - ?\n");
	scanf("%d %d", &row2, &col2);

	int** matr1 = matr_alloc(row1, col1);
	int** matr2 = matr_alloc(row2, col2);
			
	add_elements_in_matr(matr1, row1, col1);
	add_elements_in_matr(matr2, row2, col2);

	print_matr(matr1, row1, col1);
	print_matr(matr2, row2, col2);

	int menu;
	while(1)
	{
		printf("Menu:\n1. switch elements of 1-st matr\n2. multiply elements\n");
		scanf("%d", &menu);

			switch(menu)
		{	
			case 1:
			{
				printf("switching elements of 1-st matr:");
				switch_elements(matr1, row1, col1);
				print_matr(matr1, row1, col1);
				break;
			}
			case 2:
			{
				printf("multiplying elements\n");
				multiplying_elements(matr1, matr2, row1, col1, row2, col2);
				break;
			}
			default:
			{
				printf("wrong command\n");
				break;
			}
		}

	}
	

	
	return 0;
}

