// задание номер 2 (структуры + указатели) : 
//есть массив с оценками, есть поля 
//1) за физику 2) за математику 3) за русский +
//кол-во учеников вводится с клав. +
//одна ф-ция считает среднеарифметич. оценку по каждому предмету (среди всех уч-ков)
//другая ф-ция считает ср. оценку каждого ученика по всем предметам.

#include <stdio.h> //input output
#include <stdlib.h> // calloc malloc etc.

struct stud 
{
	int phys;
	int math;
	int rus;
};

float* sr_ar(int n, struct stud* a)
{
	float* mas = NULL;
	mas = calloc(sizeof(float), 3);
	if (mas == NULL)
		printf("no memory");
	
	for (int i = 0; i < n; i++)
	{
		mas[0] = mas[0] + a[i].phys;
		mas[1]= mas[1] + a[i].math;
		mas[2] = mas[2] + a[i].rus;
	}

	mas[0] = mas[0] / n;
	mas[1] = mas[1] / n;
	mas[2] = mas[2] / n;

	return mas;
}

float* every_stud(int n, struct stud* a)
{
	float* mas = NULL;
	mas = calloc(sizeof(int), n);
	if (mas == NULL)
		printf("no memory");

	for (int i = 0; i < n; i++)
		mas[i] = (a[i].phys + a[i].math + a[i].rus) / 3;

	return mas;
}

void mark(int size, struct stud* a)
{
	for(int i = 0; i < size; i++)
	{
		printf("Введите оценку за физику, математику, русский (через пробел)\n");
		scanf("%d %d %d", &a[i].phys, &a[i].math, &a[i].rus);
	}
}

void print_mas_struct(struct stud* a, int size)
{
	for(int i = 0; i < size; i++)
		printf("phys=%d math=%d rus=%d\n", a[i].phys, a[i].math, a[i].rus);
}

void print_mas(float* a, int size)
{
	for(int i = 0; i < size; i++)
		printf("%lf ", a[i]);
	printf("\n");
}

int main()
{
	int n = 0;
	printf("Введите кол-во учеников\n");
	scanf("%d", &n);

	struct stud* array = NULL;
	array = calloc(sizeof(struct stud), n);
	if (array == NULL)
		printf("no memory");

	mark(n, array);

	print_mas_struct(array, n);

	float* x = sr_ar(n, array);

	float* y = every_stud(n, array);

	printf("среднее арифметическое:\n");
	print_mas(x, 3);

	printf("средняя оценка каждого:\n");
	print_mas(y, n);

	free(array);
	free(x);
	free(y);
}