//Создать массив из элементов типа точка. 
//Количество элементов вводится с клавиатуры. 
//Найти количество комбинаций точек, по которым можно построить треугольник. 
//Проверку существования треугольника сделать отельной функцией.

#include <stdio.h> //input output
#include <stdlib.h> // calloc malloc etc.
#include <math.h> // pow sqrt etc.
#include<stdbool.h>

struct point 
{
	int x;
	int y;
};

int otrezok(struct point* a, struct point* b)
{	
	return sqrt(pow((*a).x - (*b).x, 2) + pow((*a).y - (*b).y, 2));
}

void point_add(struct point* a, int n)
{
	for(int i = 0; i < n; i++)
	{
		printf("Введите значения x и y %d-ой точки (через пробел)\n", i + 1);
		scanf("%d %d", &a[i].x, &a[i].y);
	}
}

void print_struct_array(struct point* a, int n)
{
	for(int i = 0; i < n; i++)
		printf("Координаты %d-ой точки:\n x = %d y = %d\n", i + 1, a[i].x, a[i].y);
}

bool is_triangle(int a, int b, int c) // bool
{
	return (a + b > c && a + c > b && b + c > a);

}

int tri_count(struct point* a, int n)
{	
	int count = 0;
	for(int i = 0; i < n - 2; i++)
	{
		for(int j = i + 1; j < n - 1; j++)
		{
			for(int k = j + 1; k < n; k++)
			{
				int m = otrezok(&a[i], &a[j]);
				int b = otrezok(&a[j], &a[k]);
				int c = otrezok(&a[i], &a[k]);
				if(is_triangle(m, b, c))
					count++;
			}
		}
	}
return count;
}




int main()
{	
	int n = 0;
	printf("Введите кол-во точек\n");
	scanf("%d", &n);

	struct point* array = NULL;
	array = calloc(sizeof(struct point), n);
	if (array == NULL)
		perror("no memory"); // perror - выводится в поток ошибок

	point_add(array, n);

	print_struct_array(array, n);

	printf("count = %d\n", tri_count(array, n));

	free(array);

	return 0;
}