// задание: создать динамический массив из n элементов, на выходе - 
// получить массив только из четных элементов
#include <stdio.h>
#include <stdlib.h>



void scan_size(int* n){
	while (*n <= 0){
		printf("vvedite razmer mass\n");
		scanf("%d", n);
	}
}

void scan_mas(int* mas, int size)
{
	for (int i = 0; i < size; i++){
		printf("vvedite %d element mas\n", i + 1);
		scanf("%d", &mas[i]);
	}
}

void print_mas(int* mas, int size)
{
	for (int i = 0; i < size; i++){
		printf("%d ", mas[i]);
	}
	printf("\n");
}

void chetn_mas(int* mas, int size, int** newmas, int* newsize)
{	printf("1111\n");
	for (int i = 0; i < size; i++){
		if (mas[i] % 2 == 0)
			*newsize += 1;
	}

	printf("s = %d\n", *newsize);

	*newmas = calloc(sizeof(int), *newsize);
	if(*newmas == NULL){
		printf("net pamyati\n");
	}
	int j = 0;
	for (int i = 0; i < size; i++){
		if (mas[i] % 2 == 0){
			*newmas[j] = mas[i];
			j++;
		
		}
	}
}

int main()
{
	int n = 0;
	scan_size(&n);

	int* array = NULL;
	array = calloc(sizeof(int), n);
	if(array == NULL){
		printf("net pamyati\n");
	}
	
	int newsize = 0;
	int* newmas = NULL;

	scan_mas(array, n);
	print_mas(array, n);

	chetn_mas(array, n, &newmas, &newsize);
	print_mas(newmas, newsize);

}