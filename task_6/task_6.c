//считывание из файла
//транспонировать матрицу


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int** matr_alloc(int row, int col)
{
	int** matr = NULL;
	matr = (int**) calloc(sizeof(int*), row);
	if(matr == NULL)
		perror("error: mistake");
	for(int i = 0; i < row; i++)
		matr[i] = (int*) calloc(sizeof(int), col);
	return matr;
}


void free_matr(int** matr, int row)
{
	for(int i = 0; i < row; i++)
	{
		free(matr[i]);
	}
	free (matr);
}

void add_matr(int** matr, char* file_name, int row, int col)
{
	printf("Введите матрицу\n");
	FILE* f = fopen(file_name, "r");
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{
			fscanf(f, "%d", &matr[i][j]); 
		}
	}
	fclose(f);
}

void print_matr(int** matr, int row, int col)
{
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{
			printf("%d     ", matr[i][j]); 
		}
		printf("\n");
	}
}

void trans_matr(int** matr, int row, int col)
{
	int** matr_new = matr_alloc(col, row);
	int temp;
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
			matr_new[j][i] = matr[i][j];
	}
	print_matr(matr_new, col, row);
}

int main()
{

	int row;
	int col;
	printf("Введите кол-во строк\n");
	scanf("%d", &row);
	printf("Введите кол-во столбцов\n");
	scanf("%d", &col);

	int** matr = matr_alloc(row, col);

	add_matr(matr, "test_task_6.txt", row, col);

	print_matr(matr, row, col);

	trans_matr(matr, row, col);

	free_matr(matr, row);

	return 0;
}
// 